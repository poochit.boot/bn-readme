# THIRD PARTY INTEGRATAION

ในการเชื่อมต่อระบบ chat ของผู้ใช้กับ bot บน platform botnoi sme นั้น ผู้ใช้จะต้องทำการเพิ่ม ยิง api เพื่อส่งข้อความเข้ามาหาบอท และเปิด webhook สำหรับรอรับข้อความตอบกลับจากบอท ตามรูปด้านล่าง

![INTEGRATION FLOW](../assets/A01.png "INTEGRATION FLOW")

## การยิง api สำหรับข้อความเข้ามาที่ botnoi sme
###### ENDPOINT
```
POST: {{BOTNOI_API_GATEWAY}}/webhook/app
```
###### REQUEST BODY
```
{
   "bot_id":"5d1ae0bfbfbc97000c01c569",
   "source":{
      "source_id":"eb57f817-ae7d-4f2b-aa4d-4f7f33db6db5",
      "source_type":"user",
   },
   "sender":{
      "uid":"eb57f817-ae7d-4f2b-aa4d-4f7f33db6db5",
      "display_name":"Guest 1835",
      "profile_img_url":"https://nlp-tool-heroku.botnoinoi.com/assets/svg/profile/bot.svg"
   },
   "message":{
      "mid":"____MID____",
      "type":"postback",
      "text":"QUICK",
      "postback":{
         "data":"QUICK"
      },
      "location":{
         "address":"",
         "latitude":0,
         "longitude":0
      },
      "image":{
         "img_url":""
      },
      "timestamp":1637722530163
   }
}
```

#### FIELD DESCRIPTION

- <b>bot_id</b>: <b>(STRING | REQUIRED)</b> ID ของ bot ที่สร้างไว้ใน botnoi sme platform
- <b>source</b>: <b>(SOURCE OBJECT | REQUIRED)</b> ข้อมูลของห้องแชท ได้แก่ id ของห้องแชท, ประเภทของห้องแชท
- <b>sender</b>: <b>(SENDER OBJECT | REQUIRED)</b> ข้อมูลของผู้ที่ส่งข้อความเข้ามา ประกอบไปด้วย uid, display name, profile_img_url
- <b>message</b>: <b>(MESSAGE OBJECT | REQUIRED)</b> ข้อความที่ผู้ใช้ส่งเข้ามา
##### SOURCE OBJECT
- <b>source.source_id</b>: <b>(STRING | REQUIRED)</b> id ของห้องแชท ที่อยู่บน service ที่จะเชื่อมต่อกับ botnoi sme platform
- <b>source.source_type</b>: <b>(ENUM["user", "room", "group"] | REQUIRED)</b>  ประเภทของห้องแชท
##### SENDER OBJECT
- <b>uid</b>: <b>(STRING | REQUIRED)</b> id ของผู้ส่งข้อความ
- <b>display_name</b>: <b>(STRING | REQUIRED)</b> display name ของผู้ส่งข้อความ
##### MESSAGE OBJECT
- <b>mid</b>: <b>(STRING | REQUIRED)</b> message id ของข้อความ
- <b>type</b>: <b>(STRING | (ENUM["text", "postback", "location", "image"])</b> ชนิดของข้อความ
- <b>text</b>: <b>(STRING | REQUIRED ในกรณีที่ type เป็น text)</b> text ที่ผู้ใช้ส่งเข้ามา
- <b>timestamp</b>: <b>(INTEGER | REQUIRED)</b> เวลาที่ผู้ใช้ส่งข้อความเข้ามา
- <b>postback</b>: <b>(POSTBACK MESSAGE OBJECT | REQUIRED ในกรณีที่ type เป็น postback)</b> ข้อความ postback ที่ผู้ถูกฝังไว้กับปุ่มที่ผู้ใช้กด
- <b>location</b>: <b>(LOCATION MESSAGE OBJECT | REQUIRED ในกรณีที่ type เป็น location)</b> ข้อมูล location ที่ผู้ใช้ส่งเข้ามา
- <b>image</b>: <b>(IMAGE MESSAGE OBJECT | REQUIRED ในกรณีที่ type เป็น image)</b> ข้อมูลรูปภาพที่ผู้ใช้ส่งเข้ามา
##### POSTBACK MESSAGE OBJECT
- <b>data</b>: <b>(STRING | REQUIRED)</b> ข้อความ postback ที่ผู้ถูกฝังไว้กับปุ่มที่ผู้ใช้กด

<b>ตัวอย่าง</b>
```
{
    "data": "BTN_A_POSTBACK_MSG",
}
```

##### LOCATION MESSAGE OBJECT
- <b>address</b>: <b>(STRING | REQUIRED)</b> ชื่อสถานที่
- <b>latitude</b>: <b>(FLOAT | REQUIRED)</b> Latitude
- <b>longitude</b>: <b>(FLOAT | REQUIRED)</b> Longitude

<b>ตัวอย่าง</b>
```
{
    "address": "พัฒนาการ 30 สวนหลวง กรุงเทพมหานคร 10250 ประเทศไทย",
    "latitude": 13.7285,
    "longitude": 100.62075
}
```

##### IMAGE MESSAGE OBJECT
- <b>img_url</b>: <b>(STRING | REQUIRED)</b> URL ของรูปที่ผู้ใช้ส่งเข้ามา
```
{
    "img_url": "https://example-image.com",
}
```

## การรับข้อความที่ bot หรือ agent ตอบกลับจาก botnoi sme platform

เมื่อ bot หรือ agent บน botnoi sme platform ต้องการจะส่ง หรือตอบข้อความกลับไปที่ third party บอทน้อยจะทำการส่งข้อความข้อความไปยัง webhook ที่ผู้ใช้ระบุไว้ด้วย <b><u>method POST</u></b> และอยู่ใน format ดังนี้

###### WEBHOOK EVENT BODY
```
{
    "bot_id": "5d1ae0bfbfbc97000c01c569",
    "source":{
      "source_id":"eb57f817-ae7d-4f2b-aa4d-4f7f33db6db5",
      "source_type":"user",
   },
   "messages": [
        {
            "type": "text",
            "text": "BOT REPLY MSG",
            "timestamp": 1637722530163
        },
        {
            "type": "image",
            "image": {
                "img_url": "https://example-image.com",
            },
            "timestamp": 1637722530164
        },
        {
            "type": "quick_reply",
            "text": "QUICK REPLY QUESTION?"
            "quick_reply_choices": [
                {
                    "type": "postback",
                    "label": "CHOICE #1"
                    "data": "POST_BACK_CHOICE_1"
                }
            ],
            "timestamp": 1637722530165
        },
        {
            "type": "carousel"
            "carousel_cards": [
                {
                    "title": "CARD #1",
                    "subtitle": "CARD #1 SUBTITLE",
                    "image_url": "https://example-image.com",
                    "buttons": [
                        {
                            "type" : "web_url"
                            "data" : "https://www.google.co.th",
                            "label" : "Register Now",
                        }
                    ]
                }
            ],
            "timestamp": 1637722530166
        },
        {
            "type": "buttons",
            "button_title": "BTN #1",
            "button_actions": [
                {
                    "type" : "web_url"
                    "data" : "https://www.google.co.th",
                    "label" : "Register Now",
                }
            ],
            "timestamp": 1637722530167
        }
        {
            "type": "flex",
            "flex_contents": {
                "body" : {
                    "contents" : [ 
                        {
                            ...
                        }
                    ]
                },
                "type" : "bubble"
            },
            "timestamp": 1637722530168
        }
   ]
}
```

#### FIELD DESCRIPTION

- <b>bot_id</b>: <b>(STRING)</b> ID ของ bot ที่สร้างไว้ใน botnoi sme platform
- <b>source</b>: <b>(SOURCE OBJECT)</b> ข้อมูลของห้องแชท ได้แก่ id ของห้องแชท, ประเภทของห้องแชท
- <b>messages</b>: <b>([]MESSAGE OUT OBJECT | REQUIRED)</b> ข้อความที่ bot / agent ต้องการจะตอบผู้ใช้

##### MESSAGE OUT OBJECT
- <b>type</b>: <b>(STRING | ENUM["text", "image", "quick_reply", "buttons", "carousel", "flex"])</b> ประเภทของข้อความ
- <b>text</b>: <b>(STRING)</b> ข้อความ text ที่ bot / agent ต้องการตอบกลับ
- <b>timestamp</b>: <b>(INTEGER)</b> timestamp เวลาที่ bot / agent ตอบกลับ
- <b>image</b>: <b>(IMAGE MESSAGE OBJECT)</b> ข้อมูลรูปที่ bot / agent ต้องการตอบกลับ
- <b>quick_reply_choices</b>: <b>([]BUTTON ACTION OBJECT)</b> ตัวเลือกของ quick reply
- <b>carousel_cards</b>: <b>([]CAROUSEL CARD OBJECT)</b> card ของข้อความประเภท carousel
- <b>button_title</b>: <b>(STRING)</b> title ของข้อความประเภท buttons
- <b>button_actions</b>: <b>([]BUTTON ACTION OBJECT)</b> ปุ่มของข้อความประเภท buttons
- <b>flex_contents</b>: <b>(LINE FLEX MSG CONTENT)</b> content ของข้อความประเภท flex อ้างอิงจาก line flex message (ref: <a href="https://developers.line.biz/en/docs/messaging-api/message-types/#flex-messages">LINE FLEX MSG DOCUMENT</a>)

##### CAROUSEL CARD OBJECT
- <b>title</b>: <b>(STRING)</b> title ของ card
- <b>subtitle</b>: <b>(STRING)</b> sub title ของ card
- <b>image_url</b>: <b>(STRING)</b> url รูปของ card
- <b>buttons</b>: : <b>([]BUTTON ACTION OBJECT)</b> ปุ่มของ carousel

##### BUTTON ACTION OBJECT
- <b>type</b>: <b>(STRING | ENUM["postback", "phone", "uri"])</b> ประเภทของปุ่ม
- <b>label</b>: <b>(STRING)</b> label ของปุ่ม
- <b>data</b>: <b>(STRING)</b> ข้อมูลของปุ่ม เป็นได้ทั้งข้อความ postback, เบอร์โทร, url ขึ้นอยู่กับว่าประเภทของปุ่มคืออะไร
