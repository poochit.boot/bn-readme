# API OBJECT

obj สำหรับใช้เชื่อมต่อ platform เข้ากับ api ทีเ่ป็น method ของผู้ใช้งาน สามารถใช้งานได้ 3 แบบ ได้แก่

- api ตอบกลับมาเป็น json ปรกติ
- api ตอบกลับมาเป็น object
- api ตอบกลับมาเป็น intent

## api ตอบกลับมาเป็น json ปรกติ

ในกรณีที่ผู้เขียน api ไม่ได้ทำการระบุว่า api obj นี้ จะมี response เป็น intent หรือ object

เมื่อ bot ทำการเรียกใช้ api obj และทำการเรียกใช้งาน api
ในกรณีที่ status code == 200
platform จะทำการบันทึก json ที่ได้ api ตอบกลับ โดยที่ไม่ได้ส่งคำตอบอะไรกลับไป

ในกรณีที่ผู้ใช้ต้องการให้ bot ตอบกลับด้วย parameter ผู้ใช้
สามารถเรียกใช้ parameter ดังกล่าว ได้ตามตัวอย่าง ด้านล่าง

     <<API_APINAME.PARAMNAME>>

## api ตอบกลับมาเป็น object

ในกรณีที่ผู้ใช้ ต้องการให้ bot ทำการเรียกใช้ api object และทำการตอบกลับด้วย response ที่ได้จากการยิง object นั้น

ผู้ใช้จะต้องทำการแนบ header

    Response-Type: object

มากับ response ของ api และต้องมี response body ตรงตามที่ platform กำหนด คือ response ที่ api ตอบกลับมา ต้องเป็น json ที่มี field ชื่อ <b>line_payload</b>, <b>facebook_payload</b> โดย

- <b>line_payload</b> จะเป็น array ของ object และ object ที่อยู่ใน array จะต้องถูกต้องตาม format ของ line ( อ่านเพิ่มเติมได้ที่ https://developers.line.biz/en/reference/messaging-api/#message-objects )

- <b>facebook_payload</b> จะเป็น array ของ object และ object ที่อยู่ใน array จะต้องถูกต้องตาม format ของ facebook ( อ่านเพิ่มเติมได้ที่ https://developers.facebook.com/docs/messenger-platform/reference/send-api/ )

### ตัวอย่าง

    RESPONSE BODY: {
        "line_payload":[
            {
                "type": "text"
                "text": "hello, world!"
            },
            {
                "originalContentUrl": "https://sme-botengine-pics.s3.amazonaws.com:443/xxxxxx-xxxxx-xxxxx-xxxxx-xxxxx.jpg",
                "previewImageUrl": "https://sme-botengine-pics.s3.amazonaws.com:443/xxxxxx-xxxxx-xxxxx-xxxxx-xxxxx.jpg",
                "type": "image"
            }
        ],
        "facebook_payload":[
            {
                "text": "hello, world!"
            },
            {
                "attachment": {
                "payload": {
                    "is_reusable": true,
                    "url": "https://sme-botengine-pics.s3.amazonaws.com:443/xxxxxx-xxxxx-xxxxx-xxxxx-xxxxx.jpg"
                },
                "type": "image"
                }
            }
        ]
    }

## api ตอบกลับมาเป็น intent

ในกรณีที่ผู้ใช้ ต้องการให้ bot ทำการเรียกใช้ api object ที่เชื่อมต่อกับ api ที่มี response เป็น intent
ผู้ใช้จะต้องทำการแนบ header

    Response-Type: intent

มากับ response ของ api และต้องมี response body ตรงตามที่ platform กำหนด คือ response ที่ api ตอบกลับมา ต้องเป็น json ที่มี field ชื่อ <b>intent</b> โดย

- <b>intent</b> จะเป็น string ที่เก็บชื่อของ intent

### ตัวอย่าง

    RESPONSE BODY: {
        "intent": "INTENT_NAME"
    }

เมื่อ platform ได้รับ intent จาก api แล้ว platform จะนำ intent ดังกล่าวไปหา mapping msg ที่มี intent ตรงกัน และนำ msg ของ mapping msg ไปเป็นคำตอบของบอท
