# DEFAULT PARAM OBJECT

parameter ที่ platform set ให้

ต่อจากนี้ขอเรียกคนที่ส่งข้อความหาบอทว่าผู้ใช้นาจา

## parameter ที่เกี่ยวกับ profile ของผู้ใช้

- customer_id: ID ของคนที่ส่งข้อความหาบอท
- p_display_name: display name ของผู้ใช้
- p_profile_img_url: url รูป profile ของผู้ใช้
- p_channel: channel ของผู้ใช้ (line / line_modular / facebook)

## parameter ที่เกี่ยวกับข้อความของผู้ใช้

- keyword: ข้อความ text ล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_image_url: url ของข้อความรูปล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_package_id: Packgage ID ของข้อความ sticker ล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_sticker_id: Sticker ID ของข้อความ sticker ล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_address: ชื่อสถานที่ ของข้อความประเภท location ล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_latitude: Latitude ของข้อความประเภท location ล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_longitude: Longtitude ของข้อความประเภท location ล่าสุดที่ผู้ใช้ส่งเข้ามา
- p_intent: intent ของข้อความล่าสุดที่ผู้ใช้ส่งเข้ามา (ในกรณีที่เป็นข้อความประเภท img, sticker, location p_intent จะมีค่าเป็น mp_image, mp_location, mp_sticker ตามลำดับ)
